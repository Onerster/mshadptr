let model;
let webcam
let webcamElement = document.getElementById("webcam")
let capturing = false
let meshString;

async function capture() {
    capturing = true

    while (capturing) {
        const img = await webcam.capture();
        const predictions = await model.estimateFaces(img);

        if (predictions.length > 0) {
			
			meshString = "";
           
            let a = []; b = []; c = []
            for (let i = 0; i < predictions.length; i++) {
                const keypoints = predictions[i].mesh;
                // Log facial keypoints.
                for (let i = 0; i < keypoints.length; i++) {
                    const [x, y, z] = keypoints[i];
                    a.push(x)
                    b.push(y)
                    c.push(z)
                    
                    let strLine = "";
                    strLine = x + ";" + y + ";" + z + ";\n"
                    
                    meshString = meshString + strLine;
                }
            }

            // Plotting the mesh
            var data = [
                {
                    opacity: 0.8,
                    color: 'rgb(300,100,200)',
                    type: 'mesh3d',
                    x: a,
                    y: b,
                    z: c,
                }
            ];
            Plotly.newPlot('plot', data)

        }
    }
}

function saveMeshToFile(data, filename, type) {
	var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
	
	alert("Mesh file is generated");
}

async function main() {
    // Load the MediaPipe facemesh model.
    model = await facemesh.load();
    console.log("Model loaded")

    webcam = await tf.data.webcam(webcamElement);
    const imgtemp = await webcam.capture();
    imgtemp.dispose()

    document.getElementById("capture").addEventListener("click", function () {
        capture()
    })

    document.getElementById("stop").addEventListener("click", function () {
        capturing = false
    })
    
    document.getElementById("savemesh").addEventListener("click", function (){
		saveMeshToFile(meshString, "file.txt", "text/plain")
	})
}


main();
